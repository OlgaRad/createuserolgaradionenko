package testCreateUser;

import com.github.javafaker.Faker;
import com.github.javafaker.File;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Registration {
    Random rand = new Random();
    WebDriver driver = new FirefoxDriver();

    public String randMobilePhone() {
        String result = "38050";
        for (int i = 0; i<7; i++) {
            result+= String.valueOf(rand.nextInt(7));
        }
        return result;
    }
    String mobilePhone = randMobilePhone();

    public String randWorkPhone() {
        String result = "";
        for (int i = 0; i<7; i++) {
            result+= String.valueOf(rand.nextInt(7));
        }
        return result;
    }
    String workPhone = randWorkPhone();

    public String randomEmail() {
        String email = "user";
        String randStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder newStr = new StringBuilder();
        for (int i =0; i<5; i++) {
            int index = (int) (rand.nextFloat()*randStr.length());
            newStr.append(randStr.charAt(index));
        }
        String gotRandStr = newStr.toString();
        return email+gotRandStr+"@gmail.com";

    }
    String email = randomEmail();
    Faker faker = new Faker();
    String firstName = faker.name().firstName();
    String lastName = faker.name().lastName();

    public String password() {
        String password = "";
        String randStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder newStr = new StringBuilder();
        for (int i =0; i<10; i++) {
            int index = (int) (rand.nextFloat()*randStr.length());
            newStr.append(randStr.charAt(index));
        }
        String gotRandStr = newStr.toString();
        return password+gotRandStr;
    }
    String password = password();

    public boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Оля\\IdeaProjects\\createUser\\src\\main\\resources\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    }

    @Before
    public void precondition() {
        driver.get("https://user-data.hillel.it/html/registration.html");
    }


    @Test
    public void signUp() throws InterruptedException {

        WebElement signUp = driver.findElement(By.cssSelector(".registration"));
        signUp.click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName);
        String firstName = driver.findElement(By.cssSelector("#first_name")).getAttribute("value");
        driver.findElement(By.cssSelector("#last_name")).sendKeys(lastName);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys(workPhone);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys(mobilePhone);
        driver.findElement(By.cssSelector("#field_email")).sendKeys(email);
        String gotEmail = driver.findElement(By.cssSelector("#field_email")).getAttribute("value");
        driver.findElement(By.cssSelector("#field_password")).sendKeys(password+"A"+1);
        String gotPassw = driver.findElement(By.cssSelector("#field_password")).getAttribute("value");
        driver.findElement(By.cssSelector("#male")).click();
        driver.findElement(By.tagName("select")).sendKeys("Manager");
        driver.findElement(By.cssSelector("#button_account")).click();
        Thread.sleep(2000);

        if (isAlertPresent()) {
            driver.switchTo().alert().accept();
        }

//        Alert alert = driver.switchTo().alert();
//        alert.accept();

        driver.navigate().refresh();
        driver.findElement(By.cssSelector("#email")).sendKeys(gotEmail);
        driver.findElement(By.cssSelector("#password")).sendKeys(gotPassw);
        driver.findElement(By.cssSelector(".login_button")).click();
        driver.findElement(By.cssSelector("#employees")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(firstName);
        driver.findElement(By.cssSelector("#search")).click();
   }
}
